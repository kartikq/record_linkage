from blocking.exact_or_partial_person_match_strategy_db import ExactOrPartialPersonMatchStrategyDb
from blocking.exact_person_match_strategy_db import ExactPersonMatchStrategyDb
from blocking.partial_person_match_strategy_db import PartialPersonMatchStrategyDb
from cleaners.person_candidate_cleaner import PersonCandidateCleaner
from cleaners.person_input_cleaner import PersonInputCleaner
from constants import BColors
from filters.base_filter import BaseFilter
from record_linker import RecordLinker
from scorers.person_address_scorer import PersonAddressScorer
from scorers.person_country_scorer import PersonCountryScorer
from scorers.person_email_scorer import PersonEmailScorer
from scorers.person_name_scorer import PersonNameScorer
from scorers.sponsorid_scorer import SponsoridScorer
from utils.database import Database
from datasources.mock_datasource import MockDatasource
from datasources.csv_datasource import CSVDatasource
from datasources.db_destination_datasource import DbDestinationDatasource
import logging

__author__ = 'kartikq'


class Main(object):
    @staticmethod
    def get_all_scorers():
        return [PersonAddressScorer(), PersonCountryScorer(), PersonEmailScorer(),
                PersonNameScorer(), SponsoridScorer()]

    @staticmethod
    def get_all_blocking_strategies():
        return [ExactOrPartialPersonMatchStrategyDb(Main.get_database())]

    @staticmethod
    def get_all_input_cleaners():
        return [PersonInputCleaner()]

    @staticmethod
    def get_all_candidate_cleaners():
        return [PersonCandidateCleaner()]

    @staticmethod
    def get_database():
        return Database(host='127.0.0.1', port=3306, user='root', db='test_linker')

    @staticmethod
    def get_filter():
        return BaseFilter()

    @staticmethod
    def run():
        #csv_file = "/tmp/test.csv"
        csv_file = "/tmp/to_load/IDBM_2014-06-20_FULL_.xml.csv.filtered.csv.map.csv"
        csv_output_file = "/tmp/record_linkage_out.csv"
        header_mapping = {"person_first_name": "name_first", "person_last_name": "name_last",
                          "site_address": "address", "person_email_1_value": "email",
                          "sponsor": "source", "site_country_code": "country",
                          "study_therapeutic_area": "therapy", "person_job_type": "role",
                          "site_member_id": "siteid"}
        csv_datasource = CSVDatasource(csv_file, header_mapping, csv_output_file)
        db_datasource = DbDestinationDatasource(Main.get_database(), csv_output_file)
        record_linker = RecordLinker(Main.get_all_scorers(), Main.get_all_blocking_strategies(),
                                     Main.get_all_input_cleaners(), Main.get_all_candidate_cleaners(),
                                     csv_datasource, db_datasource)
        record_linker.run(Main.get_filter().filter)


if __name__ == '__main__':
    logging.basicConfig(format=BColors.HEADER + '%(module)s:%(levelname)s:' + BColors.ENDC + '%(message)s',
                        level=logging.WARN, filename="output.log")
    Main().run()

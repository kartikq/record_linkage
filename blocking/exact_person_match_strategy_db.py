from blocking.strategy import Strategy
import logging

__author__ = 'kartikq'


class ExactPersonMatchStrategyDb(Strategy):
    """
    Returns candidates based on an exact match of the various attributes
    finds candidates by querying the dqs database
    """

    def __init__(self, database):
        self.database = database

    def find_candidates(self, record):
        sql = self.format_sql(record)

        return self.database.query(sql)

    @staticmethod
    def format_sql(rec):
        exact_filters = []
        if rec.get('name_first'):
            exact_filters.append("name_first LIKE '%s' " % str('%;' + rec['name_first'] + ';%'))
        if rec.get('name_last'):
            exact_filters.append("name_last LIKE '%s' " % str('%;' + rec['name_last'] + ';%'))
        if rec.get('email'):
            exact_filters.append("email LIKE '%s' " % str('%;' + rec['email'] + ';%'))
        if rec.get('country'):
            exact_filters.append("country LIKE '%s' " % str('%;' + rec['country'] + ';%'))
        if rec.get('source'):
            exact_filters.append("source LIKE '%s' " % str('%;' + rec['source'] + ';%'))
        if rec.get('radius') and rec.get('radius') < 1:
            exact_filters.append(
                '(not ((ne_lat < %s) or (ne_lng < %s) or (sw_lat > %s) and (sw_lng > %s)) OR (ne_lat IS NULL)) ' % (
                    rec['sw_lat'], rec['sw_lng'], rec['ne_lat'], rec['ne_lng']))

        sql = """select *
        from tbl_canonical_persons
        where(""" + " AND ".join(exact_filters) + ")"
        logging.debug(sql)

        return sql

from blocking.strategy import Strategy
import logging

__author__ = 'kartikq'


class PartialPersonMatchStrategyDb(Strategy):
    """
    Returns candidates based on an partial match of the various attributes
    finds candidates by querying the dqs database
    """

    def __init__(self, database):
        self.database = database

    def find_candidates(self, record):
        sql = self.format_sql(record)
        if sql == "":
            return []
        else:
            return self.database.query(sql)

    @staticmethod
    def format_sql(rec):
        partial_filters = []
        qcountry = ''

        if rec.get('meta_last'):
            partial_filters.append("meta_last like '%s'" % str('%;' + rec['meta_last'] + ';%'))
        if rec.get('domain_root'):
            partial_filters.append("domain_root like '%s'" % str('%;' + rec['domain_root'] + ';%'))
        if rec.get('country'):
            qcountry = " AND country = \";" + rec['country'] + ";\""
        if rec.get('source'):
            partial_filters.append("source like '%s'" % str('%;' + rec['source'] + ';%'))
        if rec.get('sw_lng'):
            partial_filters.append(
                "(name_last LIKE '%s' AND ((((lat > %s) AND (lat < %s) AND (lng > %s) "
                "AND (lng < %s)) or (ne_lat IS NULL))))"
                % (rec['name_last'] + '%', rec['sw_lat'] - 1, rec['ne_lat'] + 1, rec['sw_lng'] - 1, rec['ne_lng'] + 1))
        if len(partial_filters) < 2:
            return ""

        sql = """SELECT * FROM tbl_canonical_persons
                WHERE (""" + " OR ".join(partial_filters) + ")" + qcountry + ";"
        logging.debug(sql)

        return sql
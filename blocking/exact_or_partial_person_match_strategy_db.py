import logging
from blocking.strategy import Strategy

__author__ = 'kartikq'


class ExactOrPartialPersonMatchStrategyDb(Strategy):

    def __init__(self, database):
        self.database = database

    def find_candidates(self, record):
        sql = self.format_sql_exact(record)
        candidates = self.database.query(sql)
        if not candidates:
            sql = self.format_sql_partial(record)
            candidates = self.database.query(sql)
            return candidates
        return candidates

    @staticmethod
    def format_sql_partial(rec):
        partial_filters = []
        qcountry = ''

        if rec.get('meta_last'):
            partial_filters.append("meta_last like '%s'" % str('%;' + rec['meta_last'].replace("'", r"\'") + ';%'))
        if rec.get('domain_root'):
            partial_filters.append("domain_root like '%s'" % str('%;' + rec['domain_root'].replace("'", r"\'") + ';%'))
        if rec.get('country'):
            qcountry = " AND country LIKE '%s' " % str('%;' + rec['country'].replace("'", r"\'") + ';%')
        if rec.get('source'):
            partial_filters.append("source like '%s'" % str('%;' + rec['source'].replace("'", r"\'") + ';%'))
        if rec.get('sw_lng'):
            partial_filters.append(
                "(name_last LIKE '%s' AND ((((lat > %s) AND (lat < %s) AND (lng > %s) "
                "AND (lng < %s)) or (ne_lat IS NULL))))"
                % (rec['name_last'] + '%', rec['sw_lat'] - 1, rec['ne_lat'] + 1, rec['sw_lng'] - 1, rec['ne_lng'] + 1))
        if len(partial_filters) < 2:
            return ""

        sql = """SELECT * FROM tbl_canonical_persons
                WHERE (""" + " OR ".join(partial_filters) + ")" + qcountry + ";"
        logging.debug(sql)

        return sql

    @staticmethod
    def format_sql_exact(rec):
        exact_filters = []
        if rec.get('name_first'):
            exact_filters.append("name_first LIKE '%s' " % str('%;' + rec['name_first'].replace("'", r"\'") + ';%'))
        if rec.get('name_last'):
            exact_filters.append("name_last LIKE '%s' " % str('%;' + rec['name_last'].replace("'", r"\'") + ';%'))
        if rec.get('email'):
            exact_filters.append("email LIKE '%s' " % str('%;' + rec['email'].replace("'", r"\'") + ';%'))
        if rec.get('country'):
            exact_filters.append("country LIKE '%s' " % str('%;' + rec['country'].replace("'", r"\'") + ';%'))
        if rec.get('source'):
            exact_filters.append("source LIKE '%s' " % str('%;' + rec['source'].replace("'", r"\'") + ';%'))
        # if rec.get('radius') and rec.get('radius') < 1:
        #     exact_filters.append(
        #         '(not ((ne_lat < %s) or (ne_lng < %s) or (sw_lat > %s) and (sw_lng > %s)) OR (ne_lat IS NULL)) ' % (
        #             rec['sw_lat'], rec['sw_lng'], rec['ne_lat'], rec['ne_lng']))

        sql = """select *
        from tbl_canonical_persons
        where(""" + " AND ".join(exact_filters) + ")"
        logging.debug(sql)

        return sql
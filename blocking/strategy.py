__author__ = 'kartikq'

from abc import ABCMeta, abstractmethod


class Strategy(object):
    __metaclass__ = ABCMeta
    """
    Abstract class defining the API that must be implemented by the various blocking strategies
    """

    @abstractmethod
    def find_candidates(self, record):
        """Implement blocking strategy to identify blocks of records that may be candidates for record linkage
        :param: record for which the block will be returned
        :return: List of candidate records
        """
        pass
# coding=utf-8
__author__ = 'kartikq'

import fuzzy
import re
import logging


class StringUtils(object):
    double_metaphone = fuzzy.DMetaphone()
    UNSUPPORTED_CHARS_METAPHONE = {'ß': 'ss', 'ñ': 'ny', 'Ñ': 'ny', 'ø': 'ur', 'Ø': 'ur', 'ö': 'oe', 'Ö': 'oe',
                                   'ão': 'ow', 'ÃO': 'ow', 'Ã©': 'e', 'Ä': 'ae', 'ä': 'ae', 'Ã': 'uh', 'ã': 'uh',
                                   'á': 'uh', 'Á': 'uh', 'ç': 's', 'Ç': 's', 'ë': 'e', 'Ë': 'e', 'â': 'a', 'Â': 'a',
                                   'À': 'a', 'à': 'a', 'ê': 'e', 'Ê': 'e', 'è': 'e', 'é': 'e', 'È': 'e', 'Î': 'i',
                                   'î': 'i', 'Ì': 'i', 'ì': 'i', 'ò': 'o', 'Ò': 'o', 'ô': 'o', 'Ô': 'o', 'ù': 'u',
                                   'Ù': 'u', 'û': 'u', 'Û': 'u',
                                   '0': ' ZERO ', '1': ' ONE PREMIERE ', '2': ' TWO DEUXIEME ',
                                   '3': ' THREE TROISIEME ', '4': ' FOUR QUATRIEME ', '5': ' FIVE CINQUIEME ',
                                   '6': ' SIX SIXTH ', '7': ' SEVEN SEPTIEME ', '8': ' EIGHT HUITIEME ',
                                   '9': ' NINE NEUFIEME ', '[^A-Za-z]': ' ',
                                   '\\bSAINT\\b': 'ST', '\\bSAINTE\\b': 'ST', '\\bSINT\\b': 'ST', 'private clinic': '',
                                   '\\bGROUP\\b': 'GRP', '\\bUNI[A-z]*': 'UNI', '\\bHOSP[A-z]*': 'HOS',
                                   '\\bCOLL\\b': 'COLLEGE', '\\bCLIN[A-z]*': 'CLIN', '\\bMEDICAL': 'MED',
                                   '\\bMAODIC[A-z]*': 'MED', '\\bRAOGION[A-z]*': 'REG', '\\bREGION[A-z]*': 'REG',
                                   '\\bMEDICIN[A-z]*': 'MED', '\\bPRAXIS': '', '\\bCENTER': 'CTR', '\\bCENTRE': 'CTR',
                                   '\\bHLTH': 'HEALTH', '\\bDR\\b': '', '\\bPROF': '', '\\bOSPEDAL[A-z]*': 'OSP',
                                   '\\bHOPIT[A-z]*': 'HOS', '\\bKRANKEN[A-z]*': 'KRANK'}
    WORDS_DELETED = ['INC', 'LLC', 'CO', 'PA', 'DI', 'THE', 'A', 'AND', 'OF', 'DA', 'Y', 'IL', 'EL', 'LOS', 'T', 'E',
                     'O', 'FOR', 'DE', 'OFFICE', 'MD', 'PC', 'GMBH', 'SARL']

    @staticmethod
    def meta_phone(str):
        metaphones = [StringUtils.double_metaphone(word.upper())[0] for word in str.split()]
        metaphones = filter(lambda x: x, metaphones)
        return ''.join(metaphones)

    @staticmethod
    def cleanup_name_metaphone(inp):
        # FIXME not sure why this method is needed, keeping the old implementation for now
        # strips out all the junk that can fill site names
        # and cleans out foreign characters so that metaphone
        # works better.
        if inp == "":
            return ""
        for key, val in StringUtils.UNSUPPORTED_CHARS_METAPHONE.iteritems():
            inp = re.sub(key, val, inp, re.I)

        for delw in StringUtils.WORDS_DELETED:
            inp = re.sub('\\b' + delw + '\\b', '', inp)

        # normalize spaces and trim
        inp = re.sub('\s+', ' ', inp)
        inp = re.sub('^\s+', '', inp)
        inp = re.sub('\s+$', '', inp)

        return inp


__author__ = 'kartikq'

import time
import logging


def time_operation(original_function):
    from functools import wraps
    @wraps(original_function)
    def new_function(*args, **kwargs):
        start = time.time()
        try:
            original_function(*args, **kwargs)
        except:
            raise
        finally:
            end = time.time()
            secs = end - start
            logging.warn(original_function.__name__ + " took " + str(secs) + " seconds")

    return new_function

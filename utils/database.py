import MySQLdb

__author__ = 'kartikq'

class Database(object):
    def __init__(self, **kwargs):
        self.db = kwargs['db']
        self.conn = MySQLdb.connect(**kwargs)
        self.conn.autocommit(True)

        self.cur = self.conn.cursor(cursorclass=MySQLdb.cursors.DictCursor)

    def query(self, query):
        self.cur.execute(query)
        return self.cur.fetchall()

    def insert(self, insert_query):
        self.cur.execute(insert_query)
        return self.cur.lastrowid

    def insert_wparams(self, insert_query, *params):
        self.cur.execute(insert_query, params)
        return self.cur.lastrowid

    def update_wparams(self, query, *params):
        self.cur.execute(query, params)


#FIXME This entire package was copied from existing Canon project
#FIXME Needs extensive cleanup

__author__ = "shahid"
# --------------------------------------------------------------------
# Copyright (c) 2014 Applied Informatics Inc. All rights reserved.
# 
# This module receives a locate request and tries to geocode it,
# initially using its local cache, and then going to Google web
# services otherwise.
# --------------------------------------------------------------------
import os
import sys

from geo_database import GeoDatabase
import re
# from dqs_packages.Utils.utils import pqv
from pygeocoder import Geocoder
from time import gmtime, strftime


class GeoGoogle:
    def __init__(self):
        self.countries = {}
        self.data = {}
        self.tbl_geo = (
            'gcid', 'address', 'locality', 'state', 'country', 'cfaddress', 'cstreet_number', 'croute', 'clocality',
            'cstate', 'cpostal_code', 'ccountry', 'clat', 'clng', 'cne_lat', 'cne_lng', 'csw_lat', 'csw_lng',
            'cquality',
            'cdate', 'cengine', 'status')
        query = "SELECT DISTINCT code_country, country FROM tbl_geo_iso_regions_countries;"
        gd = GeoDatabase()
        gd.cur.execute(query)

        for r in gd.cur.fetchall():
            self.countries[r[1].upper()] = r[0]
        # print self.countries
        gd.cur.close()
        gd.conn.close()

    @staticmethod
    def now():
        """ returns current time"""
        return strftime("%Y-%m-%d", gmtime())

    @staticmethod
    def strip_re(inp):
        # print input
        if isinstance(inp, str):
            inp = re.sub('\s+', ' ', inp)
            inp = re.sub('^\s+', '', inp)
            inp = re.sub('\s+$', '', inp)
        return inp

    @staticmethod
    def get_geo_data(address, region):
        # geocode(self, address, sensor='false', bounds='', region='', language='')
        # &region=br&client=gme-drugdevinc&sensor=false&signature=J9OrgF-SsrLe8ze049mYYGTsea
        # client='gme-drugdevinc'
        # key= 'pG4oJ50CnPvtXu-bSbeE7M75Y04='
        # gc=Geocoder(client_id=client.encode(),private_key= key.encode())

        gc = Geocoder('AIzaSyC-kRwUT-G2vooHx4-DTn2QoCP98LfGZU8')
        try:
            data = gc.geocode(address=address, region=region)
            return data.raw
        except Exception as e:
            print("EX", e)
            # return [{'formatted_address': 'Avenida Princesa Isabel, 914 - Barra, Salvador - Bahia, 40130-030, Brazil', 'address_components': [{'types': ['street_number'], 'long_name': '914', 'short_name': '914'}, {'types': ['route'], 'long_name': 'Avenida Princesa Isabel', 'short_name': 'Av. Princesa Isabel'}, {'types': ['neighborhood', 'political'], 'long_name': 'Barra', 'short_name': 'Barra'}, {'types': ['locality', 'political'], 'long_name': 'Salvador', 'short_name': 'Salvador'}, {'types': ['administrative_area_level_2', 'political'], 'long_name': 'Salvador', 'short_name': 'Salvador'}, {'types': ['administrative_area_level_1', 'political'], 'long_name': 'Bahia', 'short_name': 'BA'}, {'types': ['country', 'political'], 'long_name': 'Brazil', 'short_name': 'BR'}, {'types': ['postal_code'], 'long_name': '40130-030', 'short_name': '40130-030'}], 'partial_match': True, 'geometry': {'location': {'lng': -38.5320221, 'lat': -13.0029264}, 'bounds': {'southwest': {'lng': -38.5320284, 'lat': -13.0029433}, 'northeast': {'lng': -38.5320221, 'lat': -13.0029264}}, 'viewport': {'southwest': {'lng': -38.5333742302915, 'lat': -13.0042838302915}, 'northeast': {'lng': -38.5306762697085, 'lat': -13.0015858697085}}, 'location_type': 'RANGE_INTERPOLATED'}, 'types': ['street_address']}]

    @staticmethod
    def clean_country(country):

        # Geocoders retrun inconsistent country labels
        if not country:
            country = ""
        elif re.search(' China', country, re.I):
            country = 'China'
        elif re.search('Russia', country, re.I):
            country = 'Russia'
        elif re.search('Macedonia', country, re.I):
            country = 'Macedonia'
        elif re.search('Netherlands', country, re.I):
            country = 'Netherlands'
        elif re.search('Serbia', country, re.I):
            country = 'Serbia'
        elif re.search('Viet Nam', country, re.I):
            country = 'Vietnam'
        elif re.search('Great Britain', country, re.I):
            country = 'United Kingdom'
        elif re.search('France', country, re.I):
            country = 'France'
        elif re.search('Hong.Kong', country, re.I):
            country = 'Hong Kong'
        elif re.search('Korea, South', country, re.I):
            country = 'South Korea'
        elif re.search('Macedonia', country, re.I):
            country = 'Macedonia'
        elif re.search('^Netherlands', country, re.I):
            country = 'Netherlands'
        elif re.search('^Venezuela', country, re.I):
            country = 'Venezuela'
        elif re.search('^Col.mbia', country, re.I):
            country = 'Colombia'
        elif country == 'USA':
            country = 'United States'
        elif country == 'Korea':
            country = 'South Korea'
        elif re.search('Columbia', country, re.I):
            country = 'Colombia'
        return country

    def locate(self, data):
        # data is dictionary with keys country and address

        if not data['country']:  # we can't do much without a country
            return -9

            # BRUTE FORCE CLEAN-UP INPUTS -----------------------------------------------

        data['country'] = self.clean_country(data['country'])

        # print (data['address'] + "test")

        # exceptional handling for Puerto Rico
        if data['address'].upper() == 'PUERTO RICO':
            data['address'] += ', United States'
            data['country'] = 'United States'
            data['country'] = 'Puerto Rico'

        data['address'] = self.strip_re(data['address'])
        data['address'] = re.sub(',.*?level.*?,', '', data['address'])
        data['address'] = re.sub(',.*?\blvl\b.*?', '', data['address'])
        data['address'] = re.sub(',.*?\bpiso\b.*?', '', data['address'])
        data['address'] = re.sub('^FL \d\w\w', '', data['address'])
        data['address'] = re.sub('^FL \d\d\w\w', '', data['address'])
        data['address'] = re.sub('^FL \d', '', data['address'])
        data['address'] = re.sub('\bDistrito.*,', ',', data['address'])
        data['address'] = re.sub('GREATER POLAND', '', data['address'])
        data['address'] = re.sub('KUIAVIA-POMERANIA', '', data['address'])
        data['address'] = re.sub('LOWER SILESIA', '', data['address'])
        data['address'] = re.sub('UPPER SILESIA', '', data['address'])
        data['address'] = re.sub('LESSER POLAND', '', data['address'])
        data['address'] = re.sub(', ,', ',', data['address'])
        data['address'] = re.sub('#', '%23', data['address'])
        data['address'] = re.sub('\bLazio\b', '', data['address'])
        data['address'] = re.sub('\bLatium\b', '', data['address'])

        # print (data['address'] +"tt")
        # ---------------------------------------------------------------------------
        query = "SELECT * FROM tmg_geo.tbl_geo_cache WHERE address = '%s' AND country = '%s';" % (
            data["address"].upper(), data["country"].upper())
        # query = "SELECT * FROM tmg_geo.tbl_geo_cache WHERE address = PUERTO RICO\, UNITED STATES
        # AND country = PUERTO RICO;"
        results = GeoDatabase().get_data(query)
        # print results
        if results:
            record = {}
            # print ("got something",len(results))
            for result in results[0:1]:
                record = {"gcid": result[0], "address": result[1], "country": result[4], 'data': result}

            record['country'] = self.clean_country(record['country'])
            if record['country'].upper() != data['country'].upper():  # illogical code
                return -2
            # print len(record["data"])
            self.data = dict(zip(self.tbl_geo, record["data"]))
            return record["gcid"]

        else:
            # print ("Not Record Fetched!") 
            address = data['address'] + "," + data['country']
            # print data["country"]
            if data["country"].upper() in self.countries.values():
                country_code = data["country"].lower()
            else:
                country_code = self.countries[data["country"].upper()].lower()
            if not country_code:
                address = re.sub(data["country"], "", address)
            else:
                region = country_code
            georesults = self.get_geo_data(address, region)
            if not georesults:
                return -3
            status = 0
            if not len(georesults) >= 1:
                return -2
            # which is the best response?? - when google returns many
            n = 0
            best = 0
            levl = 0
            for geo_result in georesults:

                types = ','.join(geo_result["types"])

                if re.search('hospital', types, re.I) and (levl < 11):
                    levl = 11
                    best = n
                if re.search('subpremise', types, re.I) and (levl < 10):
                    levl = 10
                    best = n
                if re.search('street_address', types, re.I) and (levl < 10):
                    levl = 10
                    best = n
                if re.search('route', types, re.I) and (levl < 6):
                    levl = 6
                    best = n
                if re.search('locality', types, re.I) and (levl < 5):
                    levl = 5
                    best = n
                if re.search('postal_code', types, re.I) and (levl < 4):
                    levl = 5
                    best = n
                if re.search('administrative_area_level_3', types, re.I) and (levl < 4):
                    levl = 4
                    best = n
                if re.search('administrative_area_level_2', types, re.I) and (levl < 3):
                    levl = 3
                    best = n
                if re.search('administrative_area_level_1', types, re.I) and (levl < 2):
                    levl = 2
                    best = n
                if re.search('country', types, re.I) and (levl < 1):
                    levl = 1
                    best = n
                n += 1

            # print ("BEST: ",best,levl)
            if levl <= 0:
                return -2

            dat = {"faddress": georesults[best].get('formatted_address', '')}

            for acomp in georesults[best].get('address_components', ''):
                if 'street_number' in acomp['types']:
                    dat["street_number"] = acomp['short_name']
                if 'route' in acomp['types']:
                    dat["route"] = acomp["short_name"]  # long name shld be better
                if 'locality' in acomp['types']:
                    dat['locality'] = acomp['short_name']
                if 'administrative_area_level_1' in acomp['types']:
                    dat['state'] = acomp['long_name']
                if 'postal_code' in acomp['types']:
                    dat['postal_code'] = acomp['short_name']
                if 'country' in acomp['types']:
                    dat['country'] = acomp['long_name']

            geometry = georesults[best].get('geometry', '')
            quality = geometry.get('location_type', '')

            if quality == "ROOFTOP":
                quality = 4
            elif quality == "RANGE_INTERPOLATED":
                quality = 3
            elif quality == "GEOMETRIC_CENTER":
                quality = 2
            elif quality == "APPROXIMATE":
                quality = 1
            else:
                quality = 0

        dat['quality'] = quality
        dat['lat'] = geometry.get('location', {}).get('lat', '')
        dat['lng'] = geometry.get('location', {}).get('lng', '')
        dat['ne_lat'] = geometry['viewport']['northeast']['lat']
        dat['ne_lng'] = geometry['viewport']['northeast']['lng']
        dat['sw_lat'] = geometry['viewport']['southwest']['lat']
        dat['sw_lng'] = geometry['viewport']['southwest']['lng']
        #
        #print (dat,self.Now())
        values = (
            data['address'].upper(), data.get('locality', '').upper(), data.get('state', '').upper(), dat['country'],
            dat['faddress'], dat.get('street_number', ''), dat.get('route', ''), dat.get('locality', ''),
            dat.get('state', ''), dat['country'], dat.get('postal_code', ''), dat['lat'], dat['lng'], dat['ne_lat'],
            dat['ne_lng'], dat['sw_lat'], dat['sw_lng'], dat['quality'], self.now(), status)

        query = "INSERT INTO tmg_geo.tbl_geo_cache ( address, locality, state, country, cfaddress, cstreet_number, " \
                "croute, clocality, cstate, ccountry, cpostal_code, clat, clng, cne_lat, cne_lng, csw_lat, csw_lng, " \
                "cquality, cdate, status) VALUES ( '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'," \
                "'%s','%s','%s','%s','%s','%s','%s');" % values
        # print (query )
        inresult = GeoDatabase().insert_Data(query)
        if inresult <= 0:
            return -1
        query = "SELECT * FROM tmg_geo.tbl_geo_cache WHERE gcid = %s;" % inresult
        gc_results = GeoDatabase().get_data(query)
        for result in gc_results[0:1]:
            record = {"gcid": result[0], "address": result[1], "country": result[4], 'data': result}
            self.data = dict(zip(self.tbl_geo, record["data"]))
        return record["gcid"]

if __name__ == '__main__':
    x = GeoGoogle()
    # print (x.countries)
    # print (x.clean_country("Col.mbia"))
    print (
        '->', x.locate({"country": "BRAZIL", "address": "AV. PRINCS ISABEL, 914 - BARRA, SALVADOR, BAHIA, BRAZIL"}),
        x.data)
#FIXME This entire package was copied from existing Canon project
#FIXME Needs extensive cleanup
import MySQLdb

# import pymysql #python3

class GeoDatabase:
    """ connect tmg_geo database"""

    def __init__(self):

        self.db = "tmg_geo"
        self.conn = MySQLdb.connect(host='127.0.0.1', port=3306, user='root', passwd='', db=self.db)
        self.cur = self.conn.cursor()

    def test(self):
        self.cur.execute("SELECT * FROM tmg_geo.tbl_geo_cache;")
        for r in self.cur.fetchall():
            print (len(r))
            l = []
            for c in r:
                l.append(str(c).encode('utf-8', 'ignore'))

            print (l)

        self.cur.close()
        self.conn.close()

    def get_data(self, query):
        """ get all data back from given query"""
        # print (query)
        self.cur.execute(query)
        result = self.cur.fetchall()
        # print (type(result))
        # self.cur.close()
        # self.conn.close()
        return result

    def insert_Data(self, query, l_id=True):
        """inserts the data in table"""
        try:
            query = query  # +'SELECT LAST_INSERT_ID();'
            # print(query)
            self.cur.execute(query)
            if l_id:
                self.cur.execute('SELECT LAST_INSERT_ID();')
                llid = self.cur.fetchone()[0]
                self.conn.commit()
                # self.cur.close()
                # self.conn.close()
                return llid
                # Commit your changes in the database
                self.conn.commit()
                # self.cur.close()
                # self.conn.close()
                # return 1
        except:
            # Rollback in case there is any error
            self.conn.rollback()
            self.cur.close()
            self.conn.close()
            return -1

            # disconnect from server


if __name__ == '__main__':
    x = GeoDatabase()
# package Geo::Database;

# use strict;
# use warnings;
# use DBI;
# use Digest::SHA1;
# use constant CONNECT => [
# "DBI:mysql:tmg_geo;host=127.0.0.1:3306",
#     'root',
#     '',
#     {
#         'RaiseError' => 1,
#         'AutoCommit' => 1,
#     },
# ];

# sub connect {
#     DBI->connect_cached( @{ (CONNECT) } ) or die $DBI::errstr;
# }

# 1;

#FIXME This entire package was copied from existing Canon project
#FIXME Needs extensive cleanup

from pygeocoder import Geocoder
from geopy.distance import vincenty

import re

class google_geocode: 

    def vicenty(self, ll_a1, ll_a2):
        return vincenty(ll_a1, ll_a2).miles


    def get_Geo_data(self,address):
        #geocode(self, address, sensor='false', bounds='', region='', language='')
        #&region=br&client=gme-drugdevinc&sensor=false&signature=J9OrgF-SsrLe8ze049mYYGTsea
            # client='gme-drugdevinc'
            # key= 'pG4oJ50CnPvtXu-bSbeE7M75Y04='
            # gc=Geocoder(client_id=client.encode(),private_key= key.encode())

        gc=Geocoder('AIzaSyC-kRwUT-G2vooHx4-DTn2QoCP98LfGZU8')
        try :
            print "GGG"
            data = gc.geocode(address=address)
            return data.raw
        except Exception as e:
            print("EX" , e)

    def dict_result(self,address):

        georesults = self.get_Geo_data(address)
        if not georesults:
            return -2
        status = 0;
        if not len(georesults) >=1:
            return -2
            # which is the best response?? - when google returns many
        n    = 0;
        best = 0;
        levl = 0;
        for geo_result in georesults:

            types = ','.join(geo_result["types"])
            
            if re.search('hospital',types,re.I) and (levl<11):
                levl = 11
                best = n
            if re.search('subpremise',types,re.I) and (levl<10):
                levl = 10
                best = n
            if re.search('street_address',types,re.I) and (levl<10):
                levl = 10
                best = n
            if re.search('route',types,re.I) and (levl<6):
                levl = 6
                best = n
            if re.search('locality',types,re.I) and (levl<5):
                levl = 5
                best = n
            if re.search('postal_code',types,re.I) and (levl<4):
                levl = 5
                best = n
            if re.search('administrative_area_level_3',types,re.I) and (levl<4):
                levl = 4
                best = n
            if re.search('administrative_area_level_2',types,re.I) and (levl<3):
                levl = 3
                best = n
            if re.search('administrative_area_level_1',types,re.I) and (levl<2):
                levl = 2
                best = n
            if re.search('country',types,re.I) and (levl<1):
                levl = 1
                best = n
            n += 1

        # print ("BEST: ",best,levl)
        if levl <= 0:
            return -2
    
        dat = {"faddress": georesults[best].get('formatted_address','')}
    
        for acomp in georesults[best].get('address_components',''):
            if 'street_number' in acomp['types']:
                dat["street_number"] = acomp['short_name']
            if 'route' in acomp['types']:
                dat["route"] = acomp["short_name"]      #long name shld be better
            if 'locality' in acomp['types']:
                dat['locality'] = acomp['short_name']
            if 'administrative_area_level_1' in acomp['types']:
                dat['state'] = acomp['long_name']
            if 'postal_code' in acomp['types']:
                dat['postal_code'] = acomp['short_name']
            if 'country' in acomp['types']:
                dat['country'] = acomp['long_name']
        
        geometry = georesults[best].get('geometry','')
        quality = geometry.get('location_type','') 
        
        if quality == "ROOFTOP":
            quality = 4
        elif quality == "RANGE_INTERPOLATED":
            quality = 3
        elif quality == "GEOMETRIC_CENTER":
            quality = 2
        elif quality == "APPROXIMATE":
            quality = 1
        else:
            quality = 0

        dat['quality'] = quality
        dat['lat'] = geometry.get('location',{}).get('lat','')
        dat['lng'] = geometry.get('location',{}).get('lng','')
        dat['ne_lat'] = geometry['viewport']['northeast']['lat'] 
        dat['ne_lng'] = geometry['viewport']['northeast']['lng'] 
        dat['sw_lat'] = geometry['viewport']['southwest']['lat'] 
        dat['sw_lng'] = geometry['viewport']['southwest']['lng'] 

        return dat 

if __name__ == '__main__':
    print google_geocode().dict_result("LMC Diabetes & Endocrinology, 531 Atkinson Ave #17, Thornhill, ON L4J 8L7, Canada")
__author__ = 'kartikq'

from utils.csv.common import UnicodeWriter


class CSVWriter(object):

    def __init__(self, csv_filename):
        self.writer = UnicodeWriter(open(csv_filename, 'w+'))

    def write_row(self, row):
        self.writer.writerow(row)
__author__ = 'kartikq'

from utils.csv.common import UnicodeReader


class CSVParser:
    def __init__(self, file_to_validate, delimiter=','):

        self.csv_file = UnicodeReader(open(file_to_validate, 'rb'), delimiter=delimiter)
        self.header = self.csv_file.next()

    def next(self):
        row = self.csv_file.next()
        if row:
            return row
        else:
            return None


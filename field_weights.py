__author__ = 'kartikq'

from math import log
from exceptions import ValueError


class FieldWeights(object):
    """ Wrapper class for fields and their associated weights
    Encapsulates the following weights:
    m - the probability of a match on the variable, given the records matched are for the same individual
    u - probability of a match on the variable, given that the records matched are not for the same individual
    match weight  - m/u (logarithmic scale)
    non match weight - (1-m)/(1-u) (logarithmic scale)
    """

    def __init__(self, name, m=None, u=None, match_weight=0.0, non_match_weight=0.0):
        if name is None:
            raise ValueError("field name is required")

        if (m is not None) and (u is not None):
            mw = log(m / u, 2)
            nmw = log((1 - m) / (1 - u), 2)
        else:
            mw = match_weight
            nmw = non_match_weight

        self.name = name
        self.m = m
        self.u = u
        self.match_weight = mw
        self.non_match_weight = nmw
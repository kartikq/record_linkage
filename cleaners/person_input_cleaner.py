from cleaners.cleaner import Cleaner
import re
from utils.geo.geo_google import GeoGoogle
from utils.string_utils import StringUtils
from constants import POSTAL_ABBREVIATIONS, COMMON_DOMAINS

__author__ = 'kartikq'


class PersonInputCleaner(Cleaner):
    """
    Class that cleans up person records
    """

    # FIXME: There is probably a better way to do this for now lets just replicate the old approach


    GEOCODER = GeoGoogle()

    def clean(self, record):
        rec = record.copy()
        rec = PersonInputCleaner.cleanup_lastname(rec)
        rec = PersonInputCleaner.cleanup_firstname(rec)
        rec = PersonInputCleaner.cleanup_email(rec)
        rec = PersonInputCleaner.add_domain_root(rec)
        rec = PersonInputCleaner.cleanup_and_geocode_address(rec)
        rec = PersonInputCleaner.add_metaphones(rec)
        rec = PersonInputCleaner.add_sponsorid(rec)
        return rec

    @staticmethod
    def cleanup_lastname(rec):
        if rec.get('name_last'):
            rec['name_last'] = re.sub('\.|,', '', rec.get('name_last'))
            rec['name_last'] = rec.get('name_last').title()
            rec['name_last'] = re.sub('(ii+)', lambda m: m.group(0).upper(), rec.get('name_last'), flags=re.I)
            rec['name_last'] = rec.get('name_last').strip('.|,|;|-| ')
        return rec

    @staticmethod
    def cleanup_firstname(rec):
        if rec.get('name_first'):
            rec['name_first'] = re.sub('\.|,', '', rec.get('name_first'))
            rec['name_first'] = rec.get('name_first').strip('.|,|;|-| ')
        return rec

    @staticmethod
    def cleanup_email(rec):
        if rec.get('email'):
            rec['email'] = rec.get('email').lower().split()[0]  # sometimes other fields like mobile numbers are passed
            # with the email
            rec['email'] = re.sub('\s', '', rec.get('email'))
            if re.match("^[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6}$", rec.get("email")) is None:
                rec['email'] = ''
        return rec

    @staticmethod
    def add_domain_root(rec):
        if rec.get('email'):
            rec['domain_root'] = rec.get('email')
            rec['domain_root'] = re.sub('\s+', '', rec["domain_root"], flags=re.I)
            rec['domain_root'] = re.sub('^.*@', '', rec['domain_root'], flags=re.I)
            if rec['domain_root'].lower() in COMMON_DOMAINS:
                rec['domain_root'] = ''
        return rec

    @staticmethod
    def cleanup_and_geocode_address(rec):
        if rec.get('address') and rec.get('country') and not rec.get('cfaddress'):
            rec['cfaddress'] = rec['address']
            try:
                gcid = PersonInputCleaner.GEOCODER.locate({'address': rec['address'], 'country': rec['country']})
                if gcid > 0:
                    if PersonInputCleaner.GEOCODER.data.get('cquality') > 1 \
                            and PersonInputCleaner.GEOCODER.data['csw_lng'] \
                            and PersonInputCleaner.GEOCODER.data['ccountry'] == rec['country']:
                        rec['cfaddress'] = PersonInputCleaner.GEOCODER.data['cfaddress']
                        rec['state'] = PersonInputCleaner.GEOCODER.data['cstate']
                        rec['quality'] = PersonInputCleaner.GEOCODER.data['cquality']
                        rec['country'] = PersonInputCleaner.GEOCODER.data['ccountry']
                        rec['radius'] = (PersonInputCleaner.GEOCODER.data['cne_lng']
                                         - PersonInputCleaner.GEOCODER.data['csw_lng']) / 2
                        if rec['radius'] < 2:
                            rec['locality'] = PersonInputCleaner.GEOCODER.data['clocality']
                            rec['lat'] = PersonInputCleaner.GEOCODER.data['clat']
                            rec['lng'] = PersonInputCleaner.GEOCODER.data['clng']
                            rec['sw_lat'] = PersonInputCleaner.GEOCODER.data['csw_lat']
                            rec['ne_lat'] = PersonInputCleaner.GEOCODER.data['cne_lat']
                            rec['sw_lng'] = PersonInputCleaner.GEOCODER.data['csw_lng']
                            rec['ne_lng'] = PersonInputCleaner.GEOCODER.data['cne_lng']
            except:
                # If geocode is not found keep going
                pass
            # normalize postal abbreviations
            rec['cfaddress'] = rec['cfaddress'].replace(',', ', ')
            rec['cfaddress'] = rec['cfaddress'].strip('.|,|;|-| ')
            rec['cfaddress'] = rec['cfaddress'].decode('utf-8')
            address_parts = [POSTAL_ABBREVIATIONS[term]
                             if term in POSTAL_ABBREVIATIONS else term
                             for term in rec['cfaddress'].split()]
            rec['cfaddress'] = ' '.join(address_parts)
        return rec

    @staticmethod
    def add_metaphones(rec):
        if rec.get('name_first'):
            cleaned_name_first = StringUtils.cleanup_name_metaphone(rec['name_first'])
            rec['meta_first'] = StringUtils.meta_phone(cleaned_name_first)
        if rec.get('name_last'):
            cleaned_name_last = StringUtils.cleanup_name_metaphone(rec['name_last'])
            rec['meta_last'] = StringUtils.meta_phone(cleaned_name_last)
        return rec

    @staticmethod
    def add_sponsorid(rec):
        if rec.get('sponsor') and rec.get('person_member_id'):
            rec['sponsorid'] = rec.get('sponsor') + ':' + rec.get('person_member_id')
        return rec
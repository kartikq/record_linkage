__author__ = 'kartikq'

from abc import ABCMeta, abstractmethod

class Cleaner(object):
    __metaclass__ = ABCMeta
    """
    Abstract class defining an API for cleaning code or code that sanitizes the input records
    """

    @abstractmethod
    def clean(self, record):
        pass

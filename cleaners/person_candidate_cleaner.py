from cleaners.cleaner import Cleaner
from utils.string_utils import StringUtils
from constants import POSTAL_ABBREVIATIONS

__author__ = 'kartikq'


class PersonCandidateCleaner(Cleaner):
    def clean(self, record):
        rec = record.copy()
        rec = PersonCandidateCleaner.cleanup_lastname(rec)
        rec = PersonCandidateCleaner.cleanup_firstname(rec)
        rec = PersonCandidateCleaner.add_address(rec)
        rec = PersonCandidateCleaner.add_metaphones(rec)
        rec = PersonCandidateCleaner.cleanup_email(rec)
        return rec

    @staticmethod
    def cleanup_lastname(rec):
        if rec.get('name_last'):
            #rec['name_last'] = rec.get('name_last').strip('.|,|;|-| ')
            rec['names_last'] = rec.get('name_last').strip('.|,|;|-| ').split(';;')
        return rec

    @staticmethod
    def cleanup_firstname(rec):
        if rec.get('name_first'):
            #rec['name_first'] = rec.get('name_first').strip('.|,|;|-| ')
            rec['names_first'] = rec.get('name_first').strip('.|,|;|-| ').split(';;')
        return rec

    @staticmethod
    def add_address(rec):
        rec_address = ''
        if rec.get('locality'):
            rec_address += rec['locality'].strip(';').replace(';;', ' ') + ' '
        if rec.get('state'):
            rec_address += rec['state'].strip(';') + ' '
        if rec.get('country'):
            rec['country_cleaned'] = rec['country'].strip(';')
            rec_address += rec['country_cleaned']
        if rec_address:
            rec['cfaddress'] = rec_address
            #normalize postal abbreviations
            rec['cfaddress'] = rec['cfaddress'].replace(',', ', ')
            rec['cfaddress'] = rec['cfaddress'].strip('.|,|;|-| ')
            address_parts = [POSTAL_ABBREVIATIONS[term] if term in POSTAL_ABBREVIATIONS else term
                             for term in rec['cfaddress'].split()]
            rec['cfaddress'] = ' '.join(address_parts)
        return rec

    @staticmethod
    def add_metaphones(rec):
        # if rec.get('name_first'):
        #     name_first = rec.get('name_first').strip('.|,|;|-| ')
        #     cleaned_name_first = StringUtils.cleanup_name_metaphone(name_first)
        #     rec['meta_first'] = StringUtils.meta_phone(cleaned_name_first)
        # if rec.get('name_last'):
        #     name_last = rec.get('name_last').strip('.|,|;|-| ')
        #     cleaned_name_last = StringUtils.cleanup_name_metaphone(name_last)
        #     rec['meta_last'] = StringUtils.meta_phone(cleaned_name_last)
        if rec.get('names_first'):
            rec['meta_names_first'] = {}
            for name in rec.get('names_first'):
                cleaned_name_first = StringUtils.cleanup_name_metaphone(name)
                rec['meta_names_first'][name] = StringUtils.meta_phone(cleaned_name_first)
        if rec.get('names_last'):
            rec['meta_names_last'] = {}
            for name in rec.get('names_last'):
                cleaned_name_last = StringUtils.cleanup_name_metaphone(name)
                rec['meta_names_last'][name] = StringUtils.meta_phone(cleaned_name_last)
        return rec

    @staticmethod
    def cleanup_email(rec):
        if rec.get('email'):
            #rec['email'] = rec['email'].strip(';').replace(';;', ';').lower()
            rec['emails'] = rec['email'].split(';')
        return rec
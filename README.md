# Fuzzy Record Linkage

## Terms
* Variable - features of the record that we consider for record matching. e.g. Last name, first name etc. 
* Traits - Set of features and their values that must match in order for two records to be considered eligible for computing fuzzy matching. 

## Model 
Each variable of the dataset (e.g. last name, first name etc.) is assigned weights. 
The weights should be based on 'u' probabilities and 'm' probabilities. 
m - the probability of a match on the variable, given the records matched are for the same individual
u - probability of a match on the variable, given that the records matched are not for the same individual

|Outcome	| Proportion of links	| Proportion of non-links	| Frequency ratio	| Weight |
|---------|----------------------|--------------------------|------------------|--------|
|Match	| m = 0.95	| u ≈ 0.083	| m/u ≈ 11.4	| ln(m/u)/ln(2) ≈ 3.51 |
|Non-match |	1−m = 0.05	| 1-u ≈ 0.917	| (1-m)/(1-u) ≈ 0.0545	| ln((1-m)/(1-u))/ln(2) ≈ -4.20 |


## Algorithm

1. Blocking phase - Retrieve potential matching candidates for a record.
Can use different blocking traits to obtain the list of potential matches. Atleast one 
trait must match exactly for entities to be compared. 

Example traits: 

* LNMPDOB (last name metaphone plus DOB)
* LNMPFNPC (last name metaphone plus first name plus zip code)
* LNFN (last name metaphone plus first name)

2. Candidates comparison phase - patients returned from the blocking phase are compared
Compare records in each group. Compare all variables for each record. If the variables 'match' then add the 'match' weight. 
If the variables dont match then add the 'non-match' weight. 
3. Scoring/Evaluation phase - Add all the variable weights to compute a total score. 

Maintain two thresholds t1 and t2 (t1<t2). 
If the score: 
* <t1 - Non matched record
* >t1 but <t2 - Uncertain, needs manual review. 
* >t2 - Match

## Weights

Since the model relies upon weights for various variables they need to be computed. 
http://www.mirthcorp.com/community/wiki/download/attachments/8028287/MHSIP%20Data%20Matching%20Tech%20Description.doc?version=1&modificationDate=1261442645000&api=v2 
has a set of weights for some variables based on a sample data set. We can compute our own based on our data set as well. 

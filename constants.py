__author__ = 'kartikq'

from field_weights import FieldWeights
from enum import Enum

ENABLE_COLOR_OUTPUT = False

class BColors:
    HEADER = '\033[95m' if ENABLE_COLOR_OUTPUT else ''
    OKBLUE = '\033[94m' if ENABLE_COLOR_OUTPUT else ''
    OKGREEN = '\033[92m' if ENABLE_COLOR_OUTPUT else ''
    WARNING = '\033[93m' if ENABLE_COLOR_OUTPUT else ''
    FAIL = '\033[91m' if ENABLE_COLOR_OUTPUT else ''
    ENDC = '\033[0m' if ENABLE_COLOR_OUTPUT else ''

class Certainty(Enum):
    CERTAIN = 0
    UNCERTAIN = 1
    MISMATCH = 2


class WeightConstants(object):
    NAME_LAST = FieldWeights(name='name_last', match_weight=9.58, non_match_weight=-3.62)
#   NAME_LAST_PREFIX3 = FieldWeights(name='name_last_prefix3', match_weight=5.18, non_match_weight=-3.62)
    NAME_FIRST = FieldWeights(name='name_first', match_weight=6.69, non_match_weight=-3.27)
#   NAME_FIRST_PREFIX3 = FieldWeights(name='name_first_prefix3', match_weight=3.37, non_match_weight=-3.27)
#   MIDDLE_INITIAL = FieldWeights(name='middle_initial', match_weight=3.65)
    EMAIL = FieldWeights(name='email', match_weight=8.0, non_match_weight=-2.27)
    ADDRESS = FieldWeights(name='address', match_weight=7.0, non_match_weight=-1.0)
    COUNTRY = FieldWeights(name='country', match_weight=0.5, non_match_weight=-3.27)
#   GEO = FieldWeights(name="geo", match_weight=0.5, non_match_weight=-0.5)
    SPONSORID = FieldWeights(name="sponsor_id", match_weight=8.0, non_match_weight=-3.62)
#   VPD = FieldWeights(name="vpd", match_weight=7.0, non_match_weight=-3.62)
#   SITEID = FieldWeights(name="site_id", match_weight=7.0, non_match_weight=-3.62)

CERTAINTY_THRESHOLD = 20.0
UNCERTAINTY_THRESHOLD = 10.0

COMMON_DOMAINS = ['yahoo', 'gmail', 'hotmail', 'list.ru', 'telus.net', 'email.cz', 'bigpond.net.au', 'o2.pl',
                  'iafrica.com', 'telkomsa.net', 'cnsmail.com', 'terra.es', 'cox.net', 'free.fr', 'inbox.ru',
                  'bellnet.ca', 'onet.eu', 'inbox.lv', 'bk.ru', 'att.net', 't-online.hu', 'verizon.net',
                  'tiscali.it', 'mweb.co.za', 'interia.pl', 'mac.com', 'telefonica.net', 'fibertel.com.ar',
                  'orange.fr', 'prodigy.net.mx', 'terra.com.br', 'rambler.ru', 'bellsouth.net', 'freemail.hu',
                  'sbcglobal.net', 'web.de', 'msn.com', '163.com', '126.com', 'otenet.gr', 'rediffmail.com',
                  'comcast.net', 'seznam.cz', 'yandex.ru', 'wp.pl', 't-online.de', 'wanadoo.fr', 'mail.ru',
                  'aol.com']

POSTAL_ABBREVIATIONS = {u'WLS': u'WELLS', u'CPE': u'CAPE', u'ORCHRD': u'ORCHARD', u'CRESCENT': u'CRESCENT',
                        u'FALL': u'FALL', u'BEACH': u'BEACH', u'MSSN': u'MISSION', u'RAMP': u'RAMP',
                        u'OPAS': u'OVERPASS',
                        u'KYS': u'KEYS', u'SPG': u'SPRING', u'BYU': u'BAYOO', u'PARKWAYS': u'PARKWAYS',
                        u'COVE': u'COVE',
                        u'BYP': u'BYPASS', u'SPRINGS': u'SPRINGS', u'ISLANDS': u'ISLANDS', u'RIVER': u'RIVER',
                        u'SPUR': u'SPURS', u'JCTS': u'JUNCTIONS', u'PINES': u'PINES', u'MNRS': u'MANORS',
                        u'GROVES': u'GROVES', u'STREET': u'STREET', u'LOOP': u'LOOP', u'VIEW': u'VIEW',
                        u'CMN': u'COMMON',
                        u'CRSNT': u'CRESCENT', u'PKWYS': u'PARKWAYS', u'THROUGHWAY': u'THROUGHWAY',
                        u'SQUARE': u'SQUARE',
                        u'CSWY': u'CAUSEWAY', u'VLG': u'VILLAGE', u'VLY': u'VALLEY', u'COMMON': u'COMMON',
                        u'MTWY': u'MOTORWAY', u'GRV': u'GROVE', u'FLAT': u'FLAT', u'LOAF': u'LOAF', u'INLET': u'INLET',
                        u'UNION': u'UNION', u'BAYOO': u'BAYOO', u'DRIVES': u'DRIVES', u'BAYOU': u'BAYOO',
                        u'GRN': u'GREEN',
                        u'FERRY': u'FERRY', u'TRCE': u'TRACE', u'BLF': u'BLUFF', u'BLFS': u'BLUFFS', u'ML': u'MILL',
                        u'RADL': u'RADIAL', u'HLS': u'HILLS', u'VWS': u'VIEWS', u'MT': u'MOUNT', u'TRKS': u'TRACK',
                        u'HBRS': u'HARBORS', u'FT': u'FORT', u'GLN': u'GLEN', u'CTS': u'COURTS', u'SMT': u'SUMMIT',
                        u'STATION': u'STATION', u'BEND': u'BEND', u'CORNER': u'CORNER', u'POINT': u'POINT',
                        u'SHL': u'SHOAL', u'MDW': u'MEADOW', u'BURGS': u'BURGS', u'ESTATE': u'ESTATE',
                        u'PLAIN': u'PLAIN',
                        u'MOUNT': u'MOUNT', u'MEDOWS': u'MEADOWS', u'SPRNGS': u'SPRINGS', u'GRVS': u'GROVES',
                        u'CREEK': u'CREEK', u'SQ': u'SQUARE', u'SW': u'SW', u'ST': u'STREET', u'ALY': u'ALLEY',
                        u'TURNPIKE': u'TURNPIKE', u'OVERPASS': u'OVERPASS', u'TRLS': u'TRAIL', u'GRDNS': u'GARDENS',
                        u'GREEN': u'GREEN', u'LF': u'LOAF', u'ROADS': u'ROADS', u'LN': u'LANE', u'LK': u'LAKE',
                        u'BLUFF': u'BLUFF', u'CLIFFS': u'CLIFFS', u'FORK': u'FORK', u'FRGS': u'FORGES',
                        u'STA': u'STATION',
                        u'KEYS': u'KEYS', u'STN': u'STATION', u'RANCH': u'RANCH', u'REST': u'REST', u'FORD': u'FORD',
                        u'SHORES': u'SHORES', u'CANYON': u'CANYON', u'TUNL': u'TUNNEL', u'FORT': u'FORT',
                        u'HAVEN': u'HAVEN', u'NCK': u'NECK', u'TRL ': u'TRAIL', u'RST': u'REST', u'PIKES': u'PIKE',
                        u'GLENS': u'GLENS', u'PNE': u'PINE', u'RAPID': u'RAPID', u'GARDENS': u'GARDENS',
                        u'PIKE': u'PIKE',
                        u'EXTS': u'EXTENSIONS', u'BOTTOM': u'BOTTOM', u'LCKS': u'LOCKS', u'RD': u'ROAD',
                        u'PRT': u'PORT',
                        u'PRR': u'PRAIRIE', u'PRAIRIE': u'PRAIRIE', u'ROAD': u'ROAD', u'CRSE': u'COURSE',
                        u'CURVE': u'CURVE', u'CRST': u'CREST', u'VIA': u'VIADUCT', u'XING': u'CROSSING',
                        u'STREME': u'STREAM', u'LAKE': u'LAKE', u'TRAIL': u'TRAIL', u'BRKS': u'BROOKS',
                        u'RADIAL': u'RADIAL', u'EXPRESSWAY': u'EXPRESSWAY', u'JUNCTIONS': u'JUNCTIONS',
                        u'CLIFF': u'CLIFF',
                        u'JUNCTON': u'JUNCTION', u'PASSAGE': u'PASSAGE', u'TRAFFICWAY': u'TRAFFICWAY',
                        u'FRDS': u'FORDS',
                        u'MEADOWS': u'MEADOWS', u'HARBORS': u'HARBORS', u'RTE': u'ROUTE', u'MOUNTAIN': u'MOUNTAIN',
                        u'GREENS': u'GREENS', u'FALLS': u'FALLS', u'BRNCH': u'BRANCH', u'HILL': u'HILL',
                        u'VILLAGE': u'VILLAGE', u'PLNS': u'PLAINS', u'SHR': u'SHORE', u'VL': u'VILLE',
                        u'PLAZA': u'PLAZA',
                        u'LANE': u'LANE', u'MOTORWAY': u'MOTORWAY', u'SHRS': u'SHORES', u'CREST': u'CREST',
                        u'HIGHWAY': u'HIGHWAY', u'GLEN': u'GLEN', u'CRES': u'CRESCENT', u'OVL': u'OVAL',
                        u'FRKS': u'FORKS',
                        u'BTM': u'BOTTOM', u'CENTERS': u'CENTERS', u'CURV': u'CURVE', u'COURT': u'COURT',
                        u'ISS': u'ISLANDS', u'SPRING': u'SPRING', u'KNOLL': u'KNOLL', u'LAKES': u'LAKES',
                        u'COURTS': u'COURTS', u'EXPY': u'EXPRESSWAY', u'LAND': u'LAND', u'STREETS': u'STREETS',
                        u'MISSION': u'MISSION', u'CAUSEWAY': u'CAUSEWAY', u'VISTA': u'VISTA', u'UNS': u'UNIONS',
                        u'FRG': u'FORGE', u'FRD': u'FORD', u'FRK': u'FORK', u'CLF': u'CLIFF', u'CLB': u'CLUB',
                        u'SKYWAY': u'SKYWAY', u'FRY': u'FERRY', u'BOULV': u'BOULEVARD', u'HVN': u'HAVEN',
                        u'KEY': u'KEY',
                        u'KY': u'KEY', u'FLTS': u'FLATS', u'BRIDGE': u'BRIDGE', u'DL': u'DALE', u'DM': u'DAM',
                        u'EXTENSION': u'EXTENSION', u'ESTATES': u'ESTATES', u'ISLND': u'ISLAND', u'DV': u'DIVIDE',
                        u'PATH': u'PATH', u'DR': u'DRIVE', u'VALLEYS': u'VALLEYS', u'CAMP': u'CAMP', u'RPD': u'RAPID',
                        u'LOOPS': u'LOOP', u'CYN': u'CANYON', u'RAPIDS': u'RAPIDS', u'HOLW': u'HOLLOW',
                        u'RNCHS': u'RANCH',
                        u'HOLLOW': u'HOLLOW', u'MLS': u'MILLS', u'MILL': u'MILL', u'STRVNUE': u'STRAVENUE',
                        u'ANNEX': u'ANNEX', u'PNES': u'PINES', u'TUNNL': u'TUNNEL', u'GATEWAY': u'GATEWAY',
                        u'LGT': u'LIGHT', u'MEADOW': u'MEADOW', u'WL': u'WELL', u'EXT': u'EXTENSION', u'BGS': u'BURGS',
                        u'WELLS': u'WELLS', u'BLVD': u'BOULEVARD', u'WY': u'WAY', u'CIRCLES': u'CIRCLES',
                        u'RIV': u'RIVER',
                        u'EXTENSIONS': u'EXTENSIONS', u'NorthWest': u'NW', u'SHLS': u'SHOALS', u'PATHS': u'PATH',
                        u'KNL': u'KNOLL', u'VILLE': u'VILLE', u'VILLAGES': u'VILLAGES', u'PARKS': u'PARKS',
                        u'PASS': u'PASS', u'BND': u'BEND', u'MOUNTAINS': u'MOUNTAINS', u'RDGS': u'RIDGES',
                        u'LNDG': u'LANDING', u'LANDING': u'LANDING', u'CIRCLE': u'CIRCLE', u'LIGHT': u'LIGHT',
                        u'VLYS': u'VALLEYS', u'FREEWAY': u'FREEWAY', u'GLNS': u'GLENS', u'SHORE': u'SHORE',
                        u'CRK': u'CREEK', u'PORT': u'PORT', u'DIVIDE': u'DIVIDE', u'PR': u'PRAIRIE', u'LDG': u'LODGE',
                        u'PT': u'POINT', u'FIELDS': u'FIELDS', u'MALL': u'MALL', u'BYPASS': u'BYPASS', u'PL': u'PLACE',
                        u'MEWS': u'MEWS', u'BRANCH': u'BRANCH', u'CLUB': u'CLUB', u'XRD': u'CROSSROAD',
                        u'LODGE': u'LODGE',
                        u'UNDERPASS': u'UNDERPASS', u'NECK': u'NECK', u'TRACE': u'TRACE', u'TRACK': u'TRACK',
                        u'FRST': u'FOREST', u'STRT': u'STREET', u'RPDS': u'RAPIDS', u'STRM': u'STREAM',
                        u'STRA': u'STRAVENUE', u'ANX': u'ANNEX', u'LCK': u'LOCK', u'COR': u'CORNER', u'BCH': u'BEACH',
                        u'STREAM': u'STREAM', u'DVD': u'DIVIDE', u'MTNS': u'MOUNTAINS', u'OVAL': u'OVAL',
                        u'MANOR': u'MANOR', u'TUNNEL': u'TUNNEL', u'PKWY': u'PARKWAYS', u'HWY': u'HIGHWAY',
                        u'ORCHARD': u'ORCHARD', u'LOCK': u'LOCK', u'COVES': u'COVES', u'FIELD': u'FIELD',
                        u'WAY': u'WAY',
                        u'CP': u'CAMP', u'CV': u'COVE', u'CT': u'COURT', u'LGTS': u'LIGHTS', u'LNDNG': u'LANDING',
                        u'RUN': u'RUN', u'CIR': u'CIRCLE', u'PLZ': u'PLAZA', u'TRAK': u'TRACK', u'RUE': u'RUE',
                        u'LOCKS': u'LOCKS', u'PLN': u'PLAIN', u'TPKE': u'TURNPIKE', u'TRWY': u'THROUGHWAY',
                        u'CVS': u'COVES', u'KNOLLS': u'KNOLLS', u'LIGHTS': u'LIGHTS', u'CRCLE': u'CIRCLE',
                        u'JCT': u'JUNCTION', u'INLT': u'INLET', u'IS': u'ISLAND', u'BROOK': u'BROOK',
                        u'BROOKS': u'BROOKS',
                        u'MTN': u'MOUNTAIN', u'VW': u'VIEW', u'FLATS': u'FLATS', u'PINE': u'PINE', u'ARC': u'ARCADE',
                        u'GRDN': u'GARDEN', u'BG': u'BURG', u'HILLS': u'HILLS', u'DRS': u'DRIVES', u'PSGE': u'PASSAGE',
                        u'PARK': u'PARKS', u'DRV': u'DRIVE', u'FWY': u'FREEWAY', u'BR': u'BRANCH', u'CTRS': u'CENTERS',
                        u'JUNCTION': u'JUNCTION', u'FORKS': u'FORKS', u'HBR': u'HARBOR', u'TURNPK': u'TURNPIKE',
                        u'STS': u'STREETS', u'CTR': u'CENTER', u'SPRNG': u'SPRING', u'RVR': u'RIVER',
                        u'HOLWS': u'HOLLOW',
                        u'SUMMIT': u'SUMMIT', u'TERRACE': u'TERRACE', u'VALLEY': u'VALLEY', u'GROVE': u'GROVE',
                        u'CLFS': u'CLIFFS', u'RIDGES': u'RIDGES', u'PORTS': u'PORTS', u'VIEWS': u'VIEWS',
                        u'HARBOR': u'HARBOR', u'BYPS': u'BYPASS', u'SQS': u'SQUARES', u'FORDS': u'FORDS',
                        u'MANORS': u'MANORS', u'ISLE': u'ISLE', u'BURG': u'BURG', u'GARDEN': u'GARDEN',
                        u'FLS': u'FALLS',
                        u'FLT': u'FLAT', u'HL': u'HILL', u'AVENUE': u'AVENUE', u'FLD': u'FIELD', u'GTWY': u'GATEWAY',
                        u'CENTER': u'CENTER', u'VIS': u'VISTA', u'MNR': u'MANOR', u'PLAINS': u'PLAINS',
                        u'PTS': u'POINTS',
                        u'ROW': u'ROW', u'FORGES': u'FORGES', u'BOULEVARD': u'BOULEVARD', u'TRL': u'TRAIL',
                        u'COURSE': u'COURSE', u'PLACE': u'PLACE', u'CAPE': u'CAPE', u'VIADUCT': u'VIADUCT',
                        u'UN': u'UNION', u'HTS': u'HEIGHTS', u'SHOAL': u'SHOAL', u'CROSSING': u'CROSSING',
                        u'AVE': u'AVENUE', u'GRNS': u'GREENS', u'ROUTE': u'ROUTE', u'FLDS': u'FIELDS',
                        u'VLGS': u'VILLAGES', u'AVNUE': u'AVENUE', u'ESTS': u'ESTATES', u'FORGE': u'FORGE',
                        u'STRAVENUE': u'STRAVENUE', u'VSTA': u'VISTA', u'UPAS': u'UNDERPASS', u'WALKS': u'WALKS',
                        u'TRFY': u'TRAFFICWAY', u'TER': u'TERRACE', u'NW': u'NW', u'PRTS': u'PORTS', u'RDS': u'ROADS',
                        u'MILLS': u'MILLS', u'RDG': u'RIDGE', u'KNLS': u'KNOLLS', u'CORS': u'CORNERS',
                        u'CROSSROAD': u'CROSSROAD', u'SPURS': u'SPURS', u'CIRS': u'CIRCLES', u'WAYS': u'WAYS',
                        u'ISLAND': u'ISLAND', u'MDWS': u'MEADOWS', u'STRM ': u'STREAM', u'BRK': u'BROOK',
                        u'BRG': u'BRIDGE', u'GDN': u'GARDEN', u'SouthWest': u'SW', u'DALE': u'DALE', u'WALK': u'WALKS',
                        u'HRBOR': u'HARBOR', u'WALL': u'WALL', u'BLUFFS': u'BLUFFS', u'DRIVE': u'DRIVE',
                        u'PLZA': u'PLAZA',
                        u'MSN': u'MISSION', u'RIDGE': u'RIDGE', u'SKWY': u'SKYWAY', u'EXTNSN': u'EXTENSION',
                        u'ARCADE': u'ARCADE', u'DAM': u'DAM', u'WELL': u'WELL', u'ALLEY': u'ALLEY', u'LKS': u'LAKES',
                        u'POINTS': u'POINTS', u'FOREST': u'FOREST', u'ORCH': u'ORCHARD', u'CORNERS': u'CORNERS',
                        u'EST': u'ESTATE', u'SHOALS': u'SHOALS', u'RNCH': u'RANCH', u'HEIGHTS': u'HEIGHTS',
                        u'SQUARES': u'SQUARES', u'GDNS': u'GARDENS', u'SPGS': u'SPRINGS', u'ISLES': u'ISLE',
                        u'UNIONS': u'UNIONS'}


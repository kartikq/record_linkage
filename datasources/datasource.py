from constants import Certainty

__author__ = 'kartikq'

from abc import ABCMeta, abstractmethod


class Datasource(object):
    __metaclass__ = ABCMeta

    """
    Abstract class defining the API that must be implemented by the various input and output datasources
    """

    """
    Input datasources must implement the following methods
    """
    @abstractmethod
    def __iter__(self):
        return self

    @abstractmethod
    def next(self):
        pass

    """
    Output datasources must implement the following methods
    """
    @abstractmethod
    def write(self, input_record=None, matching_record=None, certainty=Certainty.UNCERTAIN, score=0.0):
        pass


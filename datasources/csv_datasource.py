from constants import Certainty
from datasources.datasource import Datasource
from utils.csv.csv_parser import CSVParser
from utils.csv.csv_writer import CSVWriter
import logging

__author__ = 'kartikq'


class CSVDatasource(Datasource):
    OUTPUT_HEADER = ['ROW_ID', 'NAME_LAST', 'NAME_FIRST', 'MATCHING_P_ID', 'MATCHING_NAME_LAST', 'MATCHING_NAME_FIRST',
                     'SCORE', 'CERTAINTY']

    def __init__(self, csv_input_filename="", input_header_mapping={}, csv_output_filename=""):
        """
        :param csv_filename:
        :param input_header_mapping: mapping between column names in the file to column names used by the linker
        for e.g. {'PERSON_FIRST_NAME':'name_first', ...}
        """
        if csv_input_filename:
            self.reader = CSVParser(csv_input_filename)
            self.header = [input_header_mapping[header.lower().strip()]
                           if input_header_mapping.get(header.lower().strip()) else header.lower().strip()
                           for header in self.reader.header]
        # if csv_output_filename:
        #     self.writer = CSVWriter(csv_output_filename)
        #     self.writer.write_row(CSVDatasource.OUTPUT_HEADER)

    def next(self):
        row = self.reader.next()
        logging.info(row)
        if row:
            return dict(zip(self.header, row))
        else:
            raise StopIteration

    def __iter__(self):
        return self

    def write(self, input_record={}, matching_record={}, certainty=Certainty.UNCERTAIN, score=0.0):
        try:
            self.writer.write_row([input_record.get('row_id'), input_record.get('name_last'),
                               input_record.get('name_first'), str(matching_record.get('p_id')),
                               matching_record.get('name_last'), matching_record.get('name_first'), str(score),
                               str(certainty)])
        except:
            logging.warn("Could not write, input :" + input_record.get('row_id') +
                         " Output : " + str(matching_record.get('p_id')) + " Score : " + str(score))
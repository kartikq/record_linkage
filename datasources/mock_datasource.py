from constants import Certainty
from datasources.datasource import Datasource

__author__ = 'kartikq'


class MockDatasource(Datasource):
    MOCK_DATA = []

    """
    mock implementation of a datasource
    """
    def __init__(self):
        self.data = MockDatasource.MOCK_DATA
        self.index = 0

    def __iter__(self):
        return self

    def next(self):
        try:
            result = self.data[self.index]
        except IndexError:
            raise StopIteration
        self.index += 1
        return result

    def write(self, input_record=None, matching_record=None, certainty=Certainty.UNCERTAIN, score=0.0):
        pass
        # print "Input record: " + str(input_record)
        # print "Output record: " + str(matching_record)
        # print "Certainty: " + str(certainty)
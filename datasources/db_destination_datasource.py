import logging
from constants import Certainty
from datasources.datasource import Datasource
from utils.csv.csv_writer import CSVWriter

__author__ = 'kartikq'


class DbDestinationDatasource(Datasource):
    OUTPUT_HEADER = ['RECORD_ID', 'PERSON_ID', 'PERSON_OUTCOME', 'SCORE']

    # Map of column names and a boolean indicating whether they are to be string escaped
    COLUMN_MAP = {'name_first': True, 'name_last': True, 'email': True, 'domain_root': True, 'meta_first': True,
                  'meta_last': True, 'lat': False, 'lng': False, 'locality': True, 'state': True, 'country': True,
                  'sid': True, 'therapy': True, 'source': True, 'date_create': False, 'role': True, 'sponsorid': True,
                  'siteid': True, 'ne_lat': False, 'ne_lng': False, 'sw_lat': False, 'sw_lng': False, 'status': False,
                  'calc_quartile': False, 'calc_num_studies': False, 'rpid': False}

    def __init__(self, database, csv_output_filename=""):
        self.database = database
        self.writer = CSVWriter(csv_output_filename)
        self.writer.write_row(DbDestinationDatasource.OUTPUT_HEADER)

    def next(self):
        pass

    def __iter__(self):
        pass

    def _make_multivalues_str(self, s):
        return ";" + s + ";"

    def xstr(self, s):
        return "" if s is None else str(s)

    def _merge_records(self, input_record, match_record):
        record  = match_record.copy()
        for field, is_string in DbDestinationDatasource.COLUMN_MAP.iteritems():
            if field != 'date_create':
                input_val = input_record.get(field)
                match_val = record.get(field)
                if is_string:
                    if input_val not in match_val.split(";"):
                        record[field] = self.xstr(match_val) + self._make_multivalues_str(self.xstr(input_val))
                    else:
                        record[field] = self.xstr(match_val)
                else:
                    record[field] = self.xstr(input_val)

        return record

    def _insertdb(self, record):
        fields = []
        vals = []
        for field, is_string in DbDestinationDatasource.COLUMN_MAP.iteritems():
            if record.get(field):
                fields.append(field)
                if is_string:
                    vals.append(self._make_multivalues_str(self.xstr(record.get(field))))
                else:
                    vals.append('NOW()' if field == 'date_create' else self.xstr(record.get(field)))

        field_sql = '(' + ','.join(fields) + ')'
        vals_sql = '(' + ','.join(['%s'] * len(vals)) + ')'

        sql = "insert into tbl_canonical_persons " + field_sql  + " values " + vals_sql
        return self.database.insert_wparams(sql, *vals)

    def _updatedb(self, record):
        vals = []
        vals_sql = []
        for field, val in record.iteritems():
            if field in DbDestinationDatasource.COLUMN_MAP.keys() and field != 'date_create' and val:
                vals.append(self.xstr(val))
                vals_sql.append('' + field + "=%s")

        vals.append(self.xstr(record.get('p_id')))
        sql  = 'update tbl_canonical_persons set ' + ','.join(vals_sql) + ' where p_id=%s'

        try:
            results = self.database.update_wparams(sql, *vals)
        except:
            logging.warn("SQL statement: " + sql + " % " + vals)
            raise
        return results

    def write(self, input_record=None, matching_record=None, certainty=Certainty.UNCERTAIN, score=0.0):
        if (not matching_record or (certainty == Certainty.MISMATCH)) \
                or (certainty == Certainty.UNCERTAIN):
            # write to both database and write to output file
            pid = self._insertdb(input_record)
            self.writer.write_row([input_record.get('row_id'), str(pid), 'N', score])
            if certainty == Certainty.UNCERTAIN and matching_record and matching_record.get('p_id'):
                self.writer.write_row([input_record.get('row_id'),
                                       str(matching_record.get('p_id')), 'P', score])

        else:
            rec = self._merge_records(input_record, matching_record)
            self._updatedb(rec)
            self.writer.write_row([input_record.get('row_id'),
                                   str(matching_record.get('p_id')),
                                   'M', score])
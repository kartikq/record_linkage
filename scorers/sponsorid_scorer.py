from scorers.scorer import Scorer
from constants import WeightConstants

__author__ = 'kartikq'


class SponsoridScorer(Scorer):

    def score(self, record1, record2):
        if record1.get('sponsorid') and record2.get('sponsorid'):
            if record2['sponsorid'] in record1['sponsorid']:
                return WeightConstants.SPONSORID.match_weight
            else:
                return WeightConstants.SPONSORID.non_match_weight
        return 0.0

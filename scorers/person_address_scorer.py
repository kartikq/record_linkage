from fuzzywuzzy import fuzz
from constants import WeightConstants
from scorers.scorer import Scorer

__author__ = 'kartikq'


class PersonAddressScorer(Scorer):
    def score(self, record1, record2):
        if record1.get('cfaddress') and record2.get('cfaddress'):
            coverage_index_score = PersonAddressScorer.coverage_index(record1.get('cfaddress'), record1.get('cfaddress'))
            token_set_ratio = fuzz.token_set_ratio(record1.get('cfaddress'), record2.get('cfaddress'))

            total_address_score = (token_set_ratio * coverage_index_score) * 0.01
            return WeightConstants.ADDRESS.match_weight if total_address_score > 0.8 \
                else WeightConstants.ADDRESS.non_match_weight
        else:
            return 0.0

    @staticmethod
    def coverage_index(str1, str2):
        if str1 and str2:
            len_str1_tokens = len(str1.split())
            len_str2_tokens = len(str2.split())
            return float(min(len_str1_tokens, len_str2_tokens)) / max(len_str1_tokens, len_str2_tokens) \
                if max(len_str1_tokens, len_str2_tokens) > 0 else 0.0
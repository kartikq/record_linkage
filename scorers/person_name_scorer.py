from scorers.scorer import Scorer
from fuzzywuzzy import fuzz
from constants import WeightConstants

__author__ = 'kartikq'


class PersonNameScorer(Scorer):
    def score(self, record1, record2):
        first_name_score = max(PersonNameScorer.score_names(record1.get('name_first'), record1.get('meta_first'),
                                                            record2.get('names_first'),
                                                            record2.get('meta_names_first')))
        last_name_score = max(PersonNameScorer.score_names(record1.get('name_last'), record1.get('meta_last'),
                                                           record2.get('names_last'),
                                                           record2.get('meta_names_last')))

        if first_name_score > 0.8:
            first_name_weighted_score = WeightConstants.NAME_FIRST.match_weight
        elif len(record1.get('name_first')) > 1:
            first_name_weighted_score = WeightConstants.NAME_FIRST.non_match_weight
        else:
            first_name_weighted_score = 0.0

        if last_name_score > 0.8:
            last_name_weighted_score = WeightConstants.NAME_LAST.match_weight
        elif len(record1.get('name_last')) > 1:
            last_name_weighted_score = WeightConstants.NAME_LAST.non_match_weight
        else:
            last_name_weighted_score = 0.0

        return first_name_weighted_score + last_name_weighted_score

    @staticmethod
    def score_names(name, meta_name, names, meta_names):
        names_filtered = [nm for nm in names if len(nm) > 1] if names else []
        #names_filtered = names
        #and len(name) > 1
        if name and names_filtered and len(name) > 1:
            for n in names_filtered:
                if meta_names.get(n) and meta_names.get(n) == meta_name:
                    yield 1.0
                yield fuzz.token_set_ratio(name, n) * 0.01
        yield 0.0




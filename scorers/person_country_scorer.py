from scorers.scorer import Scorer
from constants import WeightConstants

__author__ = 'kartikq'


class PersonCountryScorer(Scorer):
    def score(self, record1, record2):
        if record1.get('country') and record2.get('country_cleaned'):
            if record1.get('country') == record2.get('country_cleaned'):
                return WeightConstants.COUNTRY.match_weight
            else:
                return WeightConstants.COUNTRY.non_match_weight
        return 0.0
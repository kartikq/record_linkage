__author__ = 'kartikq'

from abc import ABCMeta, abstractmethod

class Scorer(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def score(self, record1, record2):
        """
        Return a comparison score between two records based on a combination of field valies
        :param record1:
        :param record2:
        :return: comparison score
        """
        pass

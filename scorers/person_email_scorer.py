from scorers.scorer import Scorer
import re
from constants import WeightConstants

__author__ = 'kartikq'


class PersonEmailScorer(Scorer):

    def score(self, record1, record2):
        match_score = -1
        if record1.get('email') and record2.get('emails'):
                #and record2.get('email'):
            domain = record1.get('domain_root')
            for email in record2.get('emails'):
                if record1['email'] == email:
                    match_score = 1
                    break
                if domain and re.search(domain, email, flags=re.I):
                    match_score = 0

        return {-1: WeightConstants.EMAIL.non_match_weight,
                0: 0.0,
                1: WeightConstants.EMAIL.match_weight }.get(match_score)
from constants import BColors
from utils.timer import time_operation

__author__ = 'kartikq'

from matcher import Matcher
import logging


class RecordLinker(object):
    """
    Extracts records from the input data source, then runs fuzzy analysis to score matches and then
    writes the matches to the output datasource
    """

    def __init__(self, scorers, blocking_strategies, input_cleaners, candidate_cleaners,
                 input_datasource, output_datasource):
        self.blocking_strategies = blocking_strategies
        self.matcher = Matcher(scorers)
        self.input_cleaners = input_cleaners
        self.candidate_cleaners = candidate_cleaners
        self.input_datasource = input_datasource
        self.output_datasource = output_datasource

    @time_operation
    def run(self, match_filter=None):
        """
        Links records in the input datasource to candidates from the candidate data source
        :param match_filter: a method that returns a dictionary with
        :return:
        """
        for record in self.input_datasource:
            self._link_record(record, match_filter)

    @time_operation
    def _link_record(self, record, match_filter):
        record = self._clean(record, self.input_cleaners)
        logging.info(BColors.HEADER + "_" * 80 + BColors.ENDC)
        logging.info(BColors.OKGREEN + "Input record:" + BColors.ENDC + str(record))

        candidates = [blocking_strategy.find_candidates(record) for blocking_strategy in self.blocking_strategies]
        flattened_candidates = [candidate for sublist in candidates for candidate in sublist]
        cleaned_candidates = [self._clean(candidate, self.candidate_cleaners) for candidate in flattened_candidates]
        matches = self.matcher.match(record, cleaned_candidates)
        self.output_datasource.write(input_record=record, **match_filter(matches))

    def _clean(self, record, cleaners):
        if cleaners:
            for input_cleaner in cleaners:
                record = input_cleaner.clean(record)
        return record
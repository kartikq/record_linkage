__author__ = 'kartikq'

from abc import ABCMeta, abstractmethod


class Filter(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def filter(self, matches):
        """
        filters the set of matches to extract the best match and assigns confidence level (CERTAINTY) to best match
        :param matches: tuples of matching records and scores
        :return: dictionary with the keys matching_record and certainty
        """
        pass
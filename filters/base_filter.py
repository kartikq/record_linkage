from filters.filter import Filter
from constants import CERTAINTY_THRESHOLD, UNCERTAINTY_THRESHOLD, Certainty, BColors
import logging

__author__ = 'kartikq'


class BaseFilter(Filter):

    def filter(self, matches):
        result = {"matching_record": {}, "score": 0.0}

        if matches:
            self._log_matches(matches)
            #retrieve highest scoring match

            rec, score = matches[0]
            logging.info(BColors.OKGREEN + "Matching Record :" + BColors.ENDC + str(rec) )
            logging.info(BColors.OKGREEN + "Score :" + BColors.ENDC + str(score))

            result["matching_record"] = rec
            result["score"]= score
            if score >= CERTAINTY_THRESHOLD:
                result['certainty'] = Certainty.CERTAIN
            elif score >= UNCERTAINTY_THRESHOLD:
                result['certainty'] = Certainty.UNCERTAIN
            else:
                result['certainty'] = Certainty.MISMATCH

        return result

    def _log_matches(self, matches):
        if logging.DEBUG == logging.getLogger().getEffectiveLevel():
            for indx, match in enumerate(matches):
                logging.debug(BColors.WARNING + 'Index : ' + BColors.ENDC + str(indx)  + BColors.WARNING + ' match : '
                             + BColors.ENDC + str(match))

import logging

__author__ = 'kartikq'

from operator import itemgetter


class Matcher(object):
    def __init__(self, scorers=None):
        self.scorers = scorers  # dictionary of field_names to scorers for the field

    def match(self, record, candidates):
        """
        Perform fuzzy matching and return best matches
        :param record: record to perform fuzzy match on
        :return: List of record,score tuples ordered by score with the best matching record on the top
        """
        scores = [(rec, self.__score(record, rec)) for rec in candidates]
        return sorted(scores, key=itemgetter(1), reverse=True)

    def __score(self, record1, record2):
        for scorer in self.scorers:
            logging.info(str(record2.get('name_first')))
            logging.info(str(record2.get('name_last')))
            logging.info(str(scorer) + " : " + str(scorer.score(record1, record2)))

        return reduce(lambda x,y:x+y,
                      [scorer.score(record1, record2) for scorer in self.scorers])